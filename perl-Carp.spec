Name:      perl-Carp
Version:   1.50
Release:   421
Summary:   Alternative warn and die for modules	
License:   GPL-1.0-or-later OR Artistic-1.0-Perl
URL:       https://metacpan.org/release/Carp
Source0:   https://cpan.metacpan.org/authors/id/X/XS/XSAWYERX/Carp-%{version}.tar.gz

BuildArch: noarch
#prereq
BuildRequires: gcc perl(ExtUtils::MakeMaker) perl-interpreter perl-generators
#for test
BuildRequires: perl(Test::More) >= 0.47

%global __provides_exclude ^perl\\(DB\\)$
%description
The Carp routines are useful in your own modules because they act like
die() or warn(), but with a message which is more likely to be useful
to a user of your module.  In the case of cluck, confess, and longmess
that context is a summary of every call in the call-stack.  For a shorter
message you can use carp or croak which report the error as being from
where your module was called.  There is no guarantee that that is where
the error was, but it is a good educated guess.

%package_help

%prep
%autosetup -n Carp-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check 
make test TEST_VERBOSE=1

%files
%doc README
%{perl_vendorlib}/*

%files help
%doc Changes
%{_mandir}/man3/*


%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.50-421
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Aug 9 2024 dongyuzhen <dongyuzhen@h-partners.com> - 1.50-420
- correct license information

* Tue Oct 25 2022 renhongxun <renhongxun@h-partners.com> - 1.50-419
- Rebuild for next release

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.50-418
- Package init
